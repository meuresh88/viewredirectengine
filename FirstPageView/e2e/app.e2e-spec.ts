import { FirstPageViewPage } from './app.po';

describe('first-page-view App', function() {
  let page: FirstPageViewPage;

  beforeEach(() => {
    page = new FirstPageViewPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
