import { Component , OnInit} from '@angular/core';
import {Injectable, Inject} from '@angular/core';
import { Http, Response, RequestOptions, RequestMethod } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/Rx';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
  title:string
  phoneDetail:any =[]
  constructor(private http:Http){
    this.http.get("/Mocks/PhoneDetail.json")
      .subscribe(
      res=>{
        this.phoneDetail = res.json().data;
        console.log(res);
      }
    );

  }
  ngOnInit(){
    this.title = 'Phone detail';
    //called after the constructor and called  after the first ngOnChanges()
  }

}
